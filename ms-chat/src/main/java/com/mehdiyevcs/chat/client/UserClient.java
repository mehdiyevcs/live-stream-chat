package com.mehdiyevcs.chat.client;

import com.mehdiyevcs.chat.model.client.UserDto;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.List;

@Component
public class UserClient {
    private final WebClient webClient;

    @Value("${client.ms-user.url}")
    private String url;

    public UserClient(WebClient webClient) {
        this.webClient = webClient;
    }

    public Flux<UserDto> getUsers(List<Long> ids) {
        return webClient
                .get()
                .uri(uriBuilder -> uriBuilder.path(url).queryParam("ids", ids).build())
                .retrieve()
                .bodyToFlux(UserDto.class);
    }

    public Mono<UserDto> getUser(Long id) {
        return webClient
                .get()
                .uri(url.concat("/{id}"), id)
                .retrieve()
                .bodyToMono(UserDto.class);
    }
}
