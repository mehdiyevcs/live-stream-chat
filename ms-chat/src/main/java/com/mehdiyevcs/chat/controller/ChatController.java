package com.mehdiyevcs.chat.controller;

import com.mehdiyevcs.chat.model.ChatDto;
import com.mehdiyevcs.chat.model.ChatMessageDto;
import com.mehdiyevcs.chat.model.CreateChatRequest;
import com.mehdiyevcs.chat.service.ChatService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@AllArgsConstructor
@RestController
@RequestMapping("/v1/chat")
public class ChatController {

    private final ChatService chatService;

    @GetMapping
    public Flux<ChatDto> getChats(@RequestHeader("user-id") Long userId) {
        return chatService.getChats(userId);
    }

    @PostMapping
    public Mono<ChatDto> createChat(@RequestBody CreateChatRequest request,
                                    @RequestHeader("user-id") Long userId) {
        return chatService.createChat(userId, request);
    }

    @DeleteMapping("/{chatId}")
    public void deleteChat(@PathVariable String chatId,
                           @RequestHeader("user-id") Long userId) {
        //need to be implemented
    }

    /* Message Related end points */
    @GetMapping("/{chatId}/messages")
    public Flux<ChatMessageDto> getMessages(@PathVariable String chatId,
                                            @RequestHeader("user-id") Long userId) {
        return chatService.getMessages(chatId, userId);
    }

    @PostMapping("/{chatId}/messages")
    public Mono<ChatMessageDto> createMessage(@PathVariable String chatId,
                                              @RequestHeader("user-id") Long userId,
                                              @RequestParam String payload) {
        return chatService.createMessage(chatId, payload, userId);
    }

    /* To set messages as read */
    @PostMapping("/{chatId}/messages/read")
    public void setAsRead(@PathVariable String chatId,
                          @RequestHeader("user-id") Long userId) {
        //need to be implemented
    }
}
