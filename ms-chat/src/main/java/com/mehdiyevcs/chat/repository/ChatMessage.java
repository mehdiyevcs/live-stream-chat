package com.mehdiyevcs.chat.repository;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDateTime;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Document
public class ChatMessage {
    @Id
    private String id;
    private String payload;
    private Long sentBy;
    private boolean isRead;
    private LocalDateTime createdAt;
    private String chatId;
}
