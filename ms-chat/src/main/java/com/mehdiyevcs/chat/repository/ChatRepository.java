package com.mehdiyevcs.chat.repository;

import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface ChatRepository extends ReactiveMongoRepository<Chat, String> {

    Flux<Chat> findAllBySenderIdOrReceiverId(Long senderId, Long receiverId);

    Mono<Chat> findById(String id);

}
