package com.mehdiyevcs.chat.repository;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Document
public class Chat {
    @Id
    private String id;
    private Long senderId;
    private String senderFullName;
    private Long receiverId;
    private String receiverFullName;
}
