package com.mehdiyevcs.chat.model;

import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor
public class CreateChatRequest {
    private String payload;
    private Long receiverId;
}
