package com.mehdiyevcs.chat.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ChatMessageDto {
    private String id;
    private String payload;
    private Long sentBy;
    private boolean isRead;
    private LocalDateTime createdAt;
    private ChatDto chat;
}
