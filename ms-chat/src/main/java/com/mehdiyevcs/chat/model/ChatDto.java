package com.mehdiyevcs.chat.model;

import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor
public class ChatDto {
    private String id;
    private Long senderId;
    private String senderFullName;
    private Long receiverId;
    private String receiverFullName;
    //private Integer unreadCount; //For future
}
