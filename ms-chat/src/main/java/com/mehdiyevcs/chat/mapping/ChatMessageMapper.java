package com.mehdiyevcs.chat.mapping;

import com.mehdiyevcs.chat.model.ChatDto;
import com.mehdiyevcs.chat.model.ChatMessageDto;
import com.mehdiyevcs.chat.repository.ChatMessage;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper
public interface ChatMessageMapper {
    ChatMessageMapper INSTANCE = Mappers.getMapper(ChatMessageMapper.class);

    @Mapping(source = "chatMessage.id", target = "id")
    @Mapping(source = "chat", target = "chat")
    ChatMessageDto toDto(ChatMessage chatMessage, ChatDto chat);
}
