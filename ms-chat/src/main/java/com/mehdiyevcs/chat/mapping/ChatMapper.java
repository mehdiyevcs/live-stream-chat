package com.mehdiyevcs.chat.mapping;

import com.mehdiyevcs.chat.model.ChatDto;
import com.mehdiyevcs.chat.repository.Chat;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface ChatMapper {
    ChatMapper INSTANCE = Mappers.getMapper(ChatMapper.class);

    Chat toEntity(ChatDto chatDto);

    ChatDto toDto(Chat chat);
}
