package com.mehdiyevcs.chat;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MsChatApplication {

    public static void main(String[] args) {
        SpringApplication.run(MsChatApplication.class, args);
    }

}
