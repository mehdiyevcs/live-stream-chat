package com.mehdiyevcs.chat.service;

import com.mehdiyevcs.chat.client.UserClient;
import com.mehdiyevcs.chat.mapping.ChatMapper;
import com.mehdiyevcs.chat.mapping.ChatMessageMapper;
import com.mehdiyevcs.chat.model.ChatDto;
import com.mehdiyevcs.chat.model.ChatMessageDto;
import com.mehdiyevcs.chat.model.CreateChatRequest;
import com.mehdiyevcs.chat.repository.Chat;
import com.mehdiyevcs.chat.repository.ChatMessage;
import com.mehdiyevcs.chat.repository.ChatMessageRepository;
import com.mehdiyevcs.chat.repository.ChatRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
@AllArgsConstructor
public class ChatService {

    private final UserClient userClient;
    private final ChatRepository chatRepository;
    private final ChatMessageRepository chatMessageRepository;

    public Mono<ChatDto> createChat(Long userId, CreateChatRequest request) {
        var sender = userClient.getUser(userId);
        var receiver = userClient.getUser(request.getReceiverId());
        return sender.zipWith(receiver, (a, b) -> Chat.builder()
                        .senderId(a.getId())
                        .senderFullName(a.getFirstName() + a.getSecondName())
                        .receiverId(b.getId())
                        .receiverFullName(b.getFirstName() + b.getSecondName())
                        .build())
                .flatMap(chat1 ->
                        chatRepository.save(chat1).flatMap(chat -> {
                            chatMessageRepository.save(ChatMessage.builder()
                                    .sentBy(userId)
                                    .payload(request.getPayload())
                                    .isRead(false)
                                    .chatId(chat.getId())
                                    .build());
                            return Mono.just(ChatMapper.INSTANCE.toDto(chat));
                        }));
    }

    public Flux<ChatDto> getChats(Long userId) {
        return chatRepository.findAllBySenderIdOrReceiverId(userId, userId).map(ChatMapper.INSTANCE::toDto);
    }

    public Mono<ChatMessageDto> createMessage(String chatId, String payload, Long userId) {
        return chatRepository.findById(chatId)
                .flatMap(chat -> {
                    if (!chat.getReceiverId().equals(userId) && !chat.getSenderId().equals(userId)) {
                        return Mono.error(new RuntimeException("No Such chat found"));
                    }
                    return chatMessageRepository.save(ChatMessage.builder()
                                    .sentBy(userId)
                                    .payload(payload)
                                    .isRead(false)
                                    .chatId(chatId)
                                    .build())
                            .map(chatMessage -> ChatMessageMapper.INSTANCE.toDto(chatMessage,
                                    ChatMapper.INSTANCE.toDto(chat)));
                })
                .switchIfEmpty(Mono.error(new RuntimeException("No such chat found")));
    }

    public Flux<ChatMessageDto> getMessages(String chatId, Long userId) {
        return chatRepository.findById(chatId)
                .flatMapMany(chat -> {
                    if (!chat.getReceiverId().equals(userId) && !chat.getSenderId().equals(userId)) {
                        return Mono.error(new RuntimeException("No such chat found"));
                    }
                    var chatDto = ChatMapper.INSTANCE.toDto(chat);
                    return chatMessageRepository.findAllByChatId(chat.getId())
                            .map(chatMessage -> ChatMessageMapper.INSTANCE.toDto(chatMessage, chatDto));
                });
    }
}
