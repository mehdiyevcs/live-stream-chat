package com.mehdiyevcs.user.repository;

import com.mehdiyevcs.user.repository.entity.UserEntity;
import org.springframework.data.repository.reactive.ReactiveSortingRepository;

public interface UserRepository extends ReactiveSortingRepository<UserEntity, Long> {
}
