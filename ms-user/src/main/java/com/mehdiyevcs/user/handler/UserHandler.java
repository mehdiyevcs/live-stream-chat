package com.mehdiyevcs.user.handler;

import com.mehdiyevcs.user.mapper.UserMapper;
import com.mehdiyevcs.user.model.UserDto;
import com.mehdiyevcs.user.repository.UserRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import javax.validation.ConstraintViolation;
import javax.validation.Validator;
import java.util.stream.Collectors;

import static org.springframework.web.reactive.function.server.ServerResponse.notFound;
import static org.springframework.web.reactive.function.server.ServerResponse.ok;

@Slf4j
@Component
@AllArgsConstructor
public class UserHandler {

    private Validator validator;
    private UserRepository userRepository;

    private Mono<ServerResponse> buildUsersResponse(Flux<UserDto> users) {
        return ok().body(users, UserDto.class);
    }

    private Mono<ServerResponse> buildUsersResponse(Mono<UserDto> user) {
        return user.flatMap(c -> ok().body(BodyInserters.fromValue(c)))
                .switchIfEmpty(notFound().build());
    }

    private void validate(UserDto userDto) {
        var constraintViolations = validator.validate(userDto);
        log.info("constraintViolations : {}", constraintViolations);
        if (constraintViolations.size() > 0) {
            var errorMessage = constraintViolations
                    .stream()
                    .map(ConstraintViolation::getMessage)
                    .sorted()
                    .collect(Collectors.joining(","));
            throw new RuntimeException(errorMessage);
        }
    }

    public Mono<ServerResponse> getUsers() {
        var userDtos = userRepository.findAll().map(UserMapper.INSTANCE::toDto).log();
        return buildUsersResponse(userDtos);
    }

    public Mono<ServerResponse> getUser(ServerRequest serverRequest) {
        var id = Long.parseLong(serverRequest.pathVariable("id"));
        var userDto = userRepository.findById(id).map(UserMapper.INSTANCE::toDto).log();
        return buildUsersResponse(userDto);
    }

    public Mono<ServerResponse> createUser(ServerRequest request) {
        return request.bodyToMono(UserDto.class)
                .doOnNext(this::validate)
                .flatMap(userDto -> userRepository.save(UserMapper.INSTANCE.toEntity(userDto)))
                .flatMap(ServerResponse.status(HttpStatus.CREATED)::bodyValue);
    }
}
