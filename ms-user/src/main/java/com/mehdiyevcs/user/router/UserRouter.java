package com.mehdiyevcs.user.router;

import com.mehdiyevcs.user.handler.UserHandler;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.ServerResponse;

import static org.springframework.web.reactive.function.server.RequestPredicates.path;
import static org.springframework.web.reactive.function.server.RouterFunctions.route;

@Configuration
public class UserRouter {

    @Bean
    public RouterFunction<ServerResponse> usersRouter(UserHandler userHandler) {
        return route()
                .nest(path("/v1/users"),
                        builder -> builder.GET("", request -> userHandler.getUsers())
                                .GET("/{id}", userHandler::getUser)
                                .POST("", userHandler::createUser))
                .build();
    }
}
