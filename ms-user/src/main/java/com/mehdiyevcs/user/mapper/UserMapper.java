package com.mehdiyevcs.user.mapper;

import com.mehdiyevcs.user.model.UserDto;
import com.mehdiyevcs.user.repository.entity.UserEntity;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface UserMapper {

    UserMapper INSTANCE = Mappers.getMapper(UserMapper.class);

    UserDto toDto(UserEntity userEntity);

    UserEntity toEntity(UserDto userDto);
}
