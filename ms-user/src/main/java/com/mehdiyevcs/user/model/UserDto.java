package com.mehdiyevcs.user.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserDto {
    private Long id;

    @NotNull
    private String firstName;

    @NotNull
    private String secondName;

    @Email
    private String email;

    @Pattern(regexp = "[0-9]{1,16}", message = "phoneNumber must contain only digits and must not exceed 16 symbols")
    private String phoneNumber;
}
